CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 

INTRODUCTION
------------

This Views Taxonomy Parent ID from Term provides a filter for views that
display taxonomy terms. This filter works like the 'Parent Term' filter
however it takes the term id from an argument and converts it to the
parent id.

 * For a full description of the module, visit the project page:
https://www.drupal.org/project/views_taxonomy_parent_id_from_term

 * To submit bug reports and feature suggestions, or track changes:
https://www.drupal.org/project/issues/views_taxonomy_parent_id_from_term

    
REQUIREMENTS
------------

This module requires no modules outside of Drupal core.
  
 
INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
    https://www.drupal.org/node/1897420 for further information.
 
 
CONFIGURATION
-------------
 
The module has no menu or modifiable settings. There is no configuration. When
enabled, you can add the Parent ID From Term to views filter. If disabled, the
views using this filter do not convert the term id to parent id. 

 
MAINTAINERS
-----------
 
Current maintainers:
 * Farnoosh Johnson (farnoosh) - https://www.drupal.org/u/farnoosh
